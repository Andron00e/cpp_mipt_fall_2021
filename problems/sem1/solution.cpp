#include <iostream>
#include <vector>
#include <algorithm>

class StaticMap {
public:
    StaticMap(const std::vector<std::pair<int, int>>& dict) : dict_(dict) {
        sort(dict_.begin(), dict_.end());
    };

    bool FindKey(int key) {
        if (dict_.empty()) {
            return false;
        }
        int index = BinarySearchLeftmost(0, dict_.size() - 1, key);
        return dict_[index].first == key;
    };

    int Get(int key, int default_value) {
        if (dict_.empty()) {
            return default_value;
        }
        int index = BinarySearchLeftmost(0, dict_.size() - 1, key);
        if (dict_[index].first == key) {
            return dict_[index].second;
        } else {
            return default_value;
        }
    };

private:
    std::vector<std::pair<int, int>> dict_;

    int BinarySearchLeftmost(int left, int right, int search_for) {
        while (left < right) {
            int mid = (left + right) / 2;
            if (!(search_for <= dict_[mid].first)) {
                left = mid + 1;
            } else {
                right = mid;
            }
        }
        return left;
    };
};

int main() {
    int pairs_num, queries_num;
    std::cin >> pairs_num >> queries_num;
    
    std::vector<std::pair<int, int>> dict;
    for (int i = 0; i < pairs_num; ++i) {
        int key, value;
        std::cin >> key >> value;
        dict.emplace_back(key, value);
    }

    StaticMap map(dict);

    for (int i = 0; i < queries_num; ++i) {
        int key;
        std::cin >> key;
        if (map.FindKey(key)) {
            std::cout << map.Get(key, 0) << std::endl;
        } else {
            std::cout << "no such key" << std::endl;
        }
    }

    return 0;
}
